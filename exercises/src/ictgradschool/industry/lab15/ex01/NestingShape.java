package ictgradschool.industry.lab15.ex01;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by lelezhao on 5/13/17.
 */
public class NestingShape extends Shape {

    List<Shape> newShapeList = new ArrayList<>();

    public NestingShape() {
        super();
    }

    public NestingShape(int x, int y) {
        super(x,y);
    }

    public NestingShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
    }

    public NestingShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
    }

    public void move(int width, int height) {
        super.move(width, height);
        for (Iterator<Shape> it = newShapeList.iterator(); it.hasNext(); ) {
            it.next().move(fWidth, fHeight);
        }
    }

    public void paint(Painter painter) {
        painter.drawRect(fX,fY,fWidth,fHeight);
        painter.translate(fX,fY);

        for (Shape newOne: newShapeList){
            newOne.paint(painter);
        }
        painter.translate(-fX,-fY);


    }

    public void add(Shape child) throws IllegalArgumentException {
        if (child.parent !=null){
            throw new IllegalArgumentException();
        }else if (child.getHeight() + child.getY() > this.fHeight || child.getWidth() + child.getX() > this.fWidth) {
            throw new IllegalArgumentException();
        } else {
            this.newShapeList.add(child);
            child.parent = this;
        }

    }

    public void remove(Shape child){

        this.newShapeList.remove(child);
        child.parent = null;

    }

    public Shape shapeAt(int index) throws IndexOutOfBoundsException{

        if (index < 0 || index > newShapeList.size()){
            throw new IndexOutOfBoundsException();
        }


        return newShapeList.get(index);
    }

    public int shapeCount(){
        return newShapeList.size();
    }

    public int indexOf(Shape child){

        if (!newShapeList.contains(child)){
            return -1;
        }else

        return shapeCount()-1;

    }

    public boolean contains(Shape child){

        if (newShapeList.contains(child)) {

            return true;
        }else
            return false;
    }







}
