package ictgradschool.industry.lab15.ex02;

import ictgradschool.industry.lab15.ex01.NestingShape;
import ictgradschool.industry.lab15.ex01.Shape;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.io.File;
import java.util.Objects;

/**
 * Created by lelezhao on 5/13/17.
 */
public class TreeModelAdapter implements TreeModel{

    private NestingShape newAdaptee;

    public TreeModelAdapter(NestingShape root){

        newAdaptee = root;

    }

    @Override
    public Object getRoot(){
        return newAdaptee;
    }

    @Override
    public int getChildCount(Object parent){
        int result = 0;
        Shape newShape = (Shape) parent;

        if (newShape instanceof NestingShape){
            NestingShape newNestingShape = (NestingShape)newShape;
            result = newNestingShape.shapeCount();

        }

        return result;

    }

    @Override
    public Object getChild(Object parent,int index){
        Object newObject = null;
        if (parent instanceof NestingShape){
            NestingShape nestingShape = (NestingShape) parent;
            newObject = nestingShape.shapeAt(index);

        }

        return newObject;

    }

    @Override
    public int getIndexOfChild(Object parent, Object child) { // todo
        int indexOfChild = -1;

        if (parent instanceof  NestingShape){
            NestingShape nestingShape = (NestingShape)parent;
            Shape shape = (Shape)child;
            indexOfChild = nestingShape.indexOf(shape);
        }

        return indexOfChild;
    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {

    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {

    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {

    }

    @Override
    public boolean isLeaf(Object node) { // todo
        return !(node instanceof NestingShape);
    }

}
